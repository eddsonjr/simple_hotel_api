# Simple Hotel Rest API

Rest API simples relacionada a hoteis. 


## Configuração do Ambiente Virtual Python

Para criar um ambiente virutal do python, use o comando:
``` 
    python -m venv [dir_amb_virtual]

``` 

Já para atrivar o ambiente virutal, navegue na pasta que foi criada para o ambiente 
virtual e execute:
``` 
    source /bin/activate
``` 

Caso queira desativar o ambiente virtual, basta usar o comando _deactive_. 