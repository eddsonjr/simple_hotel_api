from flask import Flask
from flask_restful import  Api
from resources.hotel import Hoteis, Hotel #criando um pacote de resources de Hoteis


app = Flask(__name__)
api = Api(app) #gerencia todos os recursos e demais coisas da API


api.add_resource(Hoteis,'/hoteis')
api.add_resource(Hotel,'/hoteis/<string:hotel_id>')


if __name__ == '__main__':
    app.run(debug=True)
    

    