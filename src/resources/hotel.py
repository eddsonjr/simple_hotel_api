'''
    Pacote de resouces
    Resources de Hoteis
'''

from flask_restful import Resource, reqparse 
from  models.hotel_model import HotelModel
#reqparse - lib que serve para receber os argumentos de uma requisicao 
#pode ser usada para receber os argumentos json da requisicao



hoteis = [
    
    {
        'hotel_id': 'alpha',
        'nome': 'Alpha Hotel',
        'estrelas': 4.3,
        'diaria': 420.34,
        'cidade': 'Rio de Janeiro'
    },
    {
        'hotel_id': 'bravo',
        'nome': 'Bravo Hotel',
        'estrelas': 4.4,
        'diaria': 380.90,
        'cidade': 'Santa Catarina'
    },
    {
        'hotel_id': 'charlie',
        'nome': 'Charlie Hotel',
        'estrelas': 3.9,
        'diaria': 120.34,
        'cidade': 'Santa Catarina'
    }
]


#Resource - Essa classe sera usada como recurso da API, contendo as operacoes basicas de CRUD
class Hoteis(Resource):
    def get(self):
        return {'hoteis': hoteis} #a lib do flask_restful ja converte dict para json


class Hotel(Resource):
    
    
    #cria-se um RequestParser() para aceitar os argumentos passados via json para a 
    #requisicao.
    argumentos = reqparse.RequestParser()
        
    #a funcao add_argument() serve para sinalizar para o parser quais serao os 
    #argumentos vindos via json que serao aceitos. Neste caso estamos pegando os 
    #argumentos abaixo, mas se na requisicao for enviado um outro argumento que 
    #nao esteja mapeado, o parser ira ignorar
    argumentos.add_argument('nome')
    argumentos.add_argument('estrelas')
    argumentos.add_argument('diaria')
    argumentos.add_argument('cidade')
    
    
    #procura por um determinado hotel na lista de hoteis
    #usada no get e no put
    def findHotel(hotel_id):
        for hotel in hoteis:
            if(hotel['hotel_id'] == hotel_id):
                return hotel
            
        return None
         
    
    
    
    
    #metodos de crud
    def get(self,hotel_id):
        hotel = Hotel.findHotel(hotel_id)
        if hotel:
            return hotel
            
        return {'message': 'Hotel not found'},404 #status code para not found
    
    
    def post(self,hotel_id):        
        #cria-se aqui um dicionario contendo os dados que foram adquiridos e parseados
        dados = Hotel.argumentos.parse_args()
        
        #criando um objeto do tipo HotelModel que armazena os dados dos hoteis
        hotelObjeto = HotelModel(hotel_id, **dados)
        novoHotel = hotelObjeto.toJson() #converte o objeto para dicionario e json depois
        
        
        
        #uma vez que o json foi criado e ele simboliza um novo hotel,  ele sera adicionado
        #a lista de hoteis
        hoteis.append(novoHotel) 
        
        #retorna o hotel criado e o status code 201
        return novoHotel,201
        
    
    
    def put(self,hotel_id):
        dados = Hotel.argumentos.parse_args()
        
        
        #criando um objeto do tipo HotelModel que armazena os dados dos hoteis
        hotelObjeto = HotelModel(hotel_id, **dados)
        novoHotel = hotelObjeto.toJson() #converte o objeto para dicionario e json depois
        
        hotel = Hotel.findHotel(hotel_id)
        
        if hotel: #caso exista o hotel, atualiza todos os dados
            hotel.update(novoHotel)
            return novoHotel,200

        hoteis.append(novoHotel) #caso nao exista o hotel, cria-se um novo
        return novoHotel, 201
        
    
    
    def delete(self,hotel_id):
        global hoteis
        hoteis = [hotel for hotel in hoteis if hotel['hotel_id'] != hotel_id]
        return {'message': 'hotel deleted'}, 